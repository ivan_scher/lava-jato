(function() {
  'use strict';

  angular
    .module('lavajato', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'toastr']);

})();
