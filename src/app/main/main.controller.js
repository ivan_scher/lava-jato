(function() {
  'use strict';

  angular
    .module('lavajato')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr) {
    var vm = this;

    vm.days = [
      ['1', 'Boca'] ,
      ['2', 'Todo (a)'],
      ['3', 'Tique'],
      ['4', 'Vizinho (a)'],
      ['5','Santo (a)'],
      ['6','Diplomata'],
      ['7', 'Primo (a)'],
      ['8', 'Educador (a)'],
      ['9','Cabelo'],
      ['10', 'Nariz'],
      ['11','Cara'],
      ['12','Vampiro (a)'],
      ['13','Língua'],
      ['14','Pegador'],
      ['15','Pau'],
      ['16','Amigo'],
      ['17','Puxa Saco'],
      ['18','Filho (a)'],
      ['19','Cliente'],
      ['20','Amante'],
      ['21','Bunda'],
      ['22','Tiozão (ona)'],
      ['23','Deletor'],
      ['24','Bêbado'],
      ['25','Internauta'],
      ['26','Torcedor'],
      ['27','Caloteiro'],
      ['28','Índio'],
      ['29','Escritor'],
      ['30','Atleta'],
      ['31','Grego']
    ];
    vm.months = [['Janeiro', 'Careca'] ,
                 ['Fevereiro', 'Cagão (ona)'],
                 ['Março', 'Mole'],
                 ['Abril', 'Espertão (ona)'], 
                 ['Maio','Pingado (a)'],
                 ['Junho','Decrépito (a)'],
                 ['Julho', 'Torto (a)'],
                 ['Agosto', 'Feio (a)'],
                 ['Setembro','Gripado (a)'],
                 ['Outubro', 'Moleza'],
                 ['Novembro','Nervsinho (a)'], 
                 ['Dezembro','Lambe Saco']];
 
 
  }
})();
