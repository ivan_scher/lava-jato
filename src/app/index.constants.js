/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('lavajato')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
