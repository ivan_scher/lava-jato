(function() {
  'use strict';

  angular
    .module('lavajato')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
